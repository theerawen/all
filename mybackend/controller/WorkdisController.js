const Workdis = require('../models/workdis')
const WorkdisController = {
  dataList: [
    {
      id: 1,
      position: 'Finance & Accounting Supervisor',
      company: 'บริษัท เอเซียเสริมกิจลีสซิ่ง จำกัด (มหาชน)',
      responsibility:
                'Perform daily accounting routines such as handling invoices, payments, receipts and posting',
      characteristic: 'Age around 28 years.',
      type_of_work: 'งานประจำ',
      education: 'ปริญญาตรี',
      amount: 1,
      gender: 'ชาย/หญิง',
      salary: 'ขึ้นอยู่กับคุณสมบัติและประสบการณ์',
      experience: '1-8 ปี',
      place: 'กทม. (สาทร)',
      security: 'การฝึกอบรมและพัฒนาพนักงาน',
      contactname: 'Pornpim Samasiri (K.Poy)',
      address:
                'Human Resources Department 24th Fl., Sathorn City Tower, 175 South Sathorn Rd., Tungmahamek, Sathorn, Bangkok 10120',
      phone: '081-8908417, 02-6796262 (1419)',
      homepage: 'http://www.ask.co.th'

    },
    {
      id: 2,
      position: 'SSSSSSSSSS',
      company: 'บริษัท เทสเตอร์ จำกัด (มหาชน)',
      responsibility:
                'Perform daily accounting routines such as handling invoices, payments,...',
      characteristic:
                'Age around 30 years.',
      type_of_work:
                'งานประจำ',
      education:
                'ปริญญาตรี',
      amount: 1,
      gender:
                'ชาย/หญิง',
      salary:
                'ขึ้นอยู่กับคุณสมบัติและประสบการณ์',
      experience:
                '1-8 ปี',
      place:
                'กทม. (สาทร)',
      security:
                'การฝึกอบรมและพัฒนาพนักงาน',
      contactname:
                'Pornpim Samasiri (K.Poy)',
      address:
                'Human Resources Department 24th Fl., Sathorn City Tower, 175 South Sat...',
      phone:
                '081-8908417, 02-6796262 (1419)',
      homepage:
                'http://www.ask.co.th'
    }
  ],
  lastId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    // res.json(jobseekerController.addUser(payload))
    const user = new Workdis(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    // res.json(jobseekerController.updateUser(payload))
    try {
      const user = await Workdis.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    // res.json(jobseekerController.deleteUser(id))
    try {
      const user = await Workdis.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    // res.json(jobseekerController.getUsers())
    try {
      const users = await Workdis.find({})
      console.log(users)
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await Workdis.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = WorkdisController
