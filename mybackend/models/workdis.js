const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/mydb', { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function() {
    console.log('connect')
})
const workdescriptionSchema = new mongoose.Schema({
    position: String,
    company: String,
    responsibility: String,
    characteristic: String,
    type_of_work: String,
    education: String,
    amount: Number,
    gender: String,
    salary: String,
    experience: String,
    place: String,
    security: String,
    contactname: String,
    address: String,
    phone: String,
    homepage: String,

})


module.exports = mongoose.model('workdis', workdescriptionSchema)