const express = require('express')
const router = express.Router()
const WorkdisController = require('../controller/WorkdisController')

router.get('/', WorkdisController.getUsers)
router.get('/:id', WorkdisController.getUser)
router.post('/', WorkdisController.addUser)
router.put('/', WorkdisController.updateUser)
router.delete('/:id', WorkdisController.deleteUser)

module.exports = router
